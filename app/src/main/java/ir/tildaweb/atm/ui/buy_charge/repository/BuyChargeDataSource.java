package ir.tildaweb.atm.ui.buy_charge.repository;


import io.reactivex.Single;
import ir.tildaweb.atm.ui.buy_charge.model.BuyChargeRequest;
import ir.tildaweb.atm.ui.buy_charge.model.BuyChargeResponse;
import ir.tildaweb.atm.ui.deposit.model.DepositRequest;
import ir.tildaweb.atm.ui.deposit.model.DepositResponse;

public interface BuyChargeDataSource {

    Single<BuyChargeResponse> requestBuyCharge(BuyChargeRequest request);
}