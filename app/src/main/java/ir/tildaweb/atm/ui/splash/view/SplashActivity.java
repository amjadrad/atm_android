package ir.tildaweb.atm.ui.splash.view;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import ir.tildaweb.atm.databinding.ActivitySplashBinding;
import ir.tildaweb.atm.ui.base.BaseActivity;
import ir.tildaweb.atm.ui.signin.view.SigninActivity;
import ir.tildaweb.atm.ui.signup.view.SignupActivity;

public class SplashActivity extends BaseActivity implements View.OnClickListener {

    private ActivitySplashBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySplashBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.tvAppName.animate().translationX(0).setStartDelay(1000).setDuration(1000).start();
        binding.tvSlogan.animate().translationX(0).setStartDelay(1300).setDuration(1000).start();
        binding.btnSignin.animate().alpha(1).setStartDelay(2000).setDuration(1000).start();
        binding.btnSignup.animate().alpha(1).setStartDelay(2000).setDuration(1000).start();
        binding.btnSignin.setOnClickListener(this);
        binding.btnSignup.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == binding.btnSignin.getId()) {
            startActivity(new Intent(SplashActivity.this, SigninActivity.class));
//            finish();
        }else if (view.getId() == binding.btnSignup.getId()) {
            startActivity(new Intent(SplashActivity.this, SignupActivity.class));
//            finish();
        }
    }
}