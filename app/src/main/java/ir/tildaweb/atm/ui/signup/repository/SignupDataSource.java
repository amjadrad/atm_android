package ir.tildaweb.atm.ui.signup.repository;


import io.reactivex.Single;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;

public interface SignupDataSource {
    Single<SignupResponse> requestSignup(SignupRequest request);
}
