package ir.tildaweb.atm.ui.signin.view;


import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.core.content.res.ResourcesCompat;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;
import ir.tildaweb.atm.R;
import ir.tildaweb.atm.databinding.ActivitySigninBinding;
import ir.tildaweb.atm.ui.account_info.view.AccountInfoActivity;
import ir.tildaweb.atm.ui.base.BaseActivity;
import ir.tildaweb.atm.ui.main.view.MainActivity;
import ir.tildaweb.atm.ui.signin.model.SigninRequest;
import ir.tildaweb.atm.ui.signin.model.SigninResponse;
import ir.tildaweb.atm.ui.signin.view_model.SigninViewModel;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;
import ir.tildaweb.atm.ui.signup.view.SignupActivity;
import ir.tildaweb.atm.ui.splash.view.SplashActivity;

public class SigninActivity extends BaseActivity implements View.OnClickListener {

    private ActivitySigninBinding binding;
    private boolean isCardPin = false;
    private SigninViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySigninBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        viewModel = new SigninViewModel();
        binding.btnConfirmCart.setOnClickListener(this);
        binding.btnConfirmPin.setOnClickListener(this);
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("ورود به حساب بانکی");

        binding.etAccountNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 12) {
                    hideKeyboard(SigninActivity.this, binding.etAccountNumber);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        /*
          Pin
         */
        binding.etCartPin1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 1) {
                    binding.etCartPin2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.etCartPin2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 1) {
                    binding.etCartPin3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.etCartPin3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 1) {
                    binding.etCartPin4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.etCartPin4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 1) {
                    binding.etCartPin5.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.etCartPin5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 1) {
                    hideKeyboard(SigninActivity.this, binding.etCartPin5);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void requestSignin(SigninRequest request) {
        showLoadingFullPage();
        viewModel.requestProducts(request).subscribeOn(Schedulers.single()).observeOn(AndroidSchedulers.mainThread()).doFinally(new Action() {
            @Override
            public void run() {

            }
        }).subscribe(new SingleObserver<SigninResponse>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onSuccess(SigninResponse response) {
                dismissLoading();
                getAppPreferencesHelper().setLoginPref(true);
                getAppPreferencesHelper().setAccountNumber(binding.etAccountNumber.getText().toString().trim());
                startActivity(new Intent(SigninActivity.this, MainActivity.class));
                finishAffinity();
            }

            @Override
            public void onError(Throwable e) {
                dismissLoading();
                toast("مشکلی رخ داده است. مجددا امتحان کنید.");
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (isCardPin) {
            isCardPin = false;
            animationCards(false);
        } else {
            super.onBackPressed();
        }
    }

    private void animationCards(boolean isShowCardPin) {
        if (isShowCardPin) {
            binding.cardCartNumber.animate().translationX(2000).setDuration(1000).start();
            binding.cardCartNumber.animate().rotation(30).setDuration(1000).start();
            binding.cardPin.animate().translationX(0).setDuration(1000).start();
            binding.cardPin.animate().rotation(0).setDuration(1100).start();
        } else {
            binding.cardCartNumber.animate().translationX(0).setDuration(1000).start();
            binding.cardCartNumber.animate().rotation(0).setDuration(1000).start();
            binding.cardPin.animate().translationX(-2000).setDuration(1000).start();
            binding.cardPin.animate().rotation(-30).setDuration(1100).start();
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == binding.btnConfirmCart.getId()) {
            isCardPin = true;
            animationCards(true);
        } else if (view.getId() == binding.btnConfirmPin.getId()) {

            String accountNumber = binding.etAccountNumber.getText().toString().trim();
            String pin = binding.etCartPin1.getText().toString().trim();
            pin += binding.etCartPin2.getText().toString().trim();
            pin += binding.etCartPin3.getText().toString().trim();
            pin += binding.etCartPin4.getText().toString().trim();
            pin += binding.etCartPin5.getText().toString().trim();
            if (accountNumber.length() != 12) {
                toast("شماره حساب بایستی 12 رقم باشد.");
            } else if (pin.length() != 5) {
                toast("رمز وارد شده صحیح نمی باشد.");
            } else {
                SigninRequest request = new SigninRequest();
                request.setAccountNumber(accountNumber);
                request.setPassword(pin);
                requestSignin(request);
            }
        } else if (view.getId() == binding.toolbar.imageViewBack.getId()) {
            onBackPressed();
        }
    }
}