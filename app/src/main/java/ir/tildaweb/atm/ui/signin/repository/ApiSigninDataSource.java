package ir.tildaweb.atm.ui.signin.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.signin.model.SigninRequest;
import ir.tildaweb.atm.ui.signin.model.SigninResponse;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;

public class ApiSigninDataSource implements SigninDataSource {

    private SigninApiService apiService;

    public ApiSigninDataSource() {
        this.apiService = new SigninApiService();
    }


    @Override
    public Single<SigninResponse> requestSignin(SigninRequest request) {
        return apiService.requestSignin(request);
    }
}
