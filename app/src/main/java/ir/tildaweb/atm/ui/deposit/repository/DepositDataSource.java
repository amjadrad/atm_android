package ir.tildaweb.atm.ui.deposit.repository;


import io.reactivex.Single;
import ir.tildaweb.atm.ui.deposit.model.DepositRequest;
import ir.tildaweb.atm.ui.deposit.model.DepositResponse;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferRequest;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferResponse;

public interface DepositDataSource {

    Single<DepositResponse> requestDeposit(DepositRequest request);
}