package ir.tildaweb.atm.ui.deposit.view;


import android.os.Bundle;
import android.view.View;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;
import ir.tildaweb.atm.R;
import ir.tildaweb.atm.databinding.ActivityDepositBinding;
import ir.tildaweb.atm.dialog.DialogBottomSheetMessage;
import ir.tildaweb.atm.dialog.DialogBottomSheetMessageConfirm;
import ir.tildaweb.atm.ui.balance.model.BalanceResponse;
import ir.tildaweb.atm.ui.base.BaseActivity;
import ir.tildaweb.atm.ui.deposit.model.DepositRequest;
import ir.tildaweb.atm.ui.deposit.model.DepositResponse;
import ir.tildaweb.atm.ui.deposit.view_model.DepositViewModel;

public class DepositActivity extends BaseActivity implements View.OnClickListener {

    private ActivityDepositBinding binding;
    private DepositViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDepositBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        viewModel = new DepositViewModel();
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("واریز وجه");
        binding.btnConfirm.setOnClickListener(this);

    }

    private void showDialogConfirmPriceToDeposit(int price) {

        DialogBottomSheetMessageConfirm dialogBottomSheetMessageConfirm = new DialogBottomSheetMessageConfirm();
        dialogBottomSheetMessageConfirm.setTitle("واریز وجه");
        dialogBottomSheetMessageConfirm.setDescription(String.format("آیا مطمئن هستید که مبلغ %d دلار به حساب شما واریز شود؟", price));
        dialogBottomSheetMessageConfirm.setOnCancelClickListener(view -> dialogBottomSheetMessageConfirm.dismiss());
        dialogBottomSheetMessageConfirm.setOnConfirmClickListener(view -> {
            dialogBottomSheetMessageConfirm.dismiss();
            DepositRequest request = new DepositRequest();
            request.setAccountNumber(getAppPreferencesHelper().getAccountNumber());
            request.setAmount(price);
            requestDeposit(request);

        });
        dialogBottomSheetMessageConfirm.show(getSupportFragmentManager());

    }

    private void requestDeposit(DepositRequest request) {
        showLoadingFullPage();
        viewModel.requestDeposit(request).subscribeOn(Schedulers.single()).observeOn(AndroidSchedulers.mainThread()).doFinally(new Action() {
            @Override
            public void run() {

            }
        }).subscribe(new SingleObserver<DepositResponse>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onSuccess(DepositResponse response) {
                dismissLoading();
                DialogBottomSheetMessage dialogBottomSheetMessage = new DialogBottomSheetMessage();
                dialogBottomSheetMessage.setTitle("واریز وجه");
                dialogBottomSheetMessage.setDescription("واریز وجه با موفقیت انجام شد.");
                dialogBottomSheetMessage.setOnConfirmClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onBackPressed();
                    }
                });
                dialogBottomSheetMessage.show(getSupportFragmentManager());
//                binding.tvBalanceAvailable.setText(String.format("قابل برداشت: %s", response.getResult().getAvailableBalance()));
//                binding.tvBalance.setText(String.format("%s", response.getResult().getBalance()));
            }

            @Override
            public void onError(Throwable e) {
                dismissLoading();
                e.printStackTrace();
                toast("مشکلی رخ داده است. مجددا امتحان کنید.");
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }

            case R.id.btnConfirm: {
                try {
                    int price = Integer.parseInt(binding.etPrice.getText().toString().trim());
                    showDialogConfirmPriceToDeposit(price);
                } catch (Exception e) {
                    e.printStackTrace();
                    toast("مبلغ به درستی وارد نشده است.");
                }
                break;
            }
        }
    }
}