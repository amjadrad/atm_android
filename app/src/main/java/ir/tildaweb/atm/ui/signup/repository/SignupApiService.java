package ir.tildaweb.atm.ui.signup.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.data.network.ApiProvider;
import ir.tildaweb.atm.data.network.ApiService;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;

public class SignupApiService implements SignupDataSource {

    private ApiService apiService;

    public SignupApiService() {
        this.apiService = ApiProvider.apiProvider();
    }

    @Override
    public Single<SignupResponse> requestSignup(SignupRequest request) {
        return apiService.requestSignup(request);
    }


}
