package ir.tildaweb.atm.ui.balance.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.balance.model.BalanceRequest;
import ir.tildaweb.atm.ui.balance.model.BalanceResponse;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;

public class ApiBalanceDataSource implements BalanceDataSource {

    private BalanceApiService apiService;

    public ApiBalanceDataSource(){
        this.apiService = new BalanceApiService();
    }


    @Override
    public Single<BalanceResponse> requestAccountBalance(BalanceRequest request) {
        return apiService.requestAccountBalance(request);
    }
}
