package ir.tildaweb.atm.ui.withdrawal.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.data.network.ApiProvider;
import ir.tildaweb.atm.data.network.ApiService;
import ir.tildaweb.atm.ui.withdrawal.model.WithdrawalRequest;
import ir.tildaweb.atm.ui.withdrawal.model.WithdrawalResponse;

public class WithdrawalApiService implements WithdrawalDataSource {

    private ApiService apiService;

    public WithdrawalApiService() {
        this.apiService = ApiProvider.apiProvider();
    }


    @Override
    public Single<WithdrawalResponse> requestWithdrawal(WithdrawalRequest request) {
        return apiService.requestWithdrawal(request.getAccountNumber(), request.getAmount());
    }
}
