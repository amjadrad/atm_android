package ir.tildaweb.atm.ui.money_transfer.view_model;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferRequest;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferResponse;
import ir.tildaweb.atm.ui.money_transfer.repository.MoneyTransferRepository;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;
import ir.tildaweb.atm.ui.signup.repository.SignupRepository;

public class MoneyTransferViewModel {

    private MoneyTransferRepository repository;

    public MoneyTransferViewModel() {
        repository = new MoneyTransferRepository();
    }

    public Single<MoneyTransferResponse> requestMoneyTransfer(MoneyTransferRequest request) {
        return repository.requestMoneyTransfer(request);
    }
}
