package ir.tildaweb.atm.ui.money_transfer.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.data.network.ApiProvider;
import ir.tildaweb.atm.data.network.ApiService;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferRequest;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferResponse;
import ir.tildaweb.atm.ui.withdrawal.model.WithdrawalRequest;
import ir.tildaweb.atm.ui.withdrawal.model.WithdrawalResponse;

public class MoneyTransferApiService implements MoneyTransferDataSource {

    private ApiService apiService;

    public MoneyTransferApiService() {
        this.apiService = ApiProvider.apiProvider();
    }


    @Override
    public Single<MoneyTransferResponse> requestMoneyTransfer(MoneyTransferRequest request) {
        return apiService.requestTransfer(request.getAccountNumber(), request.getCardNumber(), request.getAmount());
    }
}
