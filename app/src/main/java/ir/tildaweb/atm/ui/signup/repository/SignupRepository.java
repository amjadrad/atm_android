package ir.tildaweb.atm.ui.signup.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;

public class SignupRepository implements SignupDataSource {

    private ApiSignupDataSource apiDataSource;

    public SignupRepository() {
        this.apiDataSource = new ApiSignupDataSource();
    }

    @Override
    public Single<SignupResponse> requestSignup(SignupRequest request) {
        return apiDataSource.requestSignup(request);
    }
}
