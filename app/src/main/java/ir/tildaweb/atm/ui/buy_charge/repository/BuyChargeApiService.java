package ir.tildaweb.atm.ui.buy_charge.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.data.network.ApiProvider;
import ir.tildaweb.atm.data.network.ApiService;
import ir.tildaweb.atm.ui.buy_charge.model.BuyChargeRequest;
import ir.tildaweb.atm.ui.buy_charge.model.BuyChargeResponse;
import ir.tildaweb.atm.ui.deposit.model.DepositRequest;
import ir.tildaweb.atm.ui.deposit.model.DepositResponse;

public class BuyChargeApiService implements BuyChargeDataSource {

    private ApiService apiService;

    public BuyChargeApiService() {
        this.apiService = ApiProvider.apiProvider();
    }

    @Override
    public Single<BuyChargeResponse> requestBuyCharge(BuyChargeRequest request) {
        return apiService.requestCharge(request);
    }
}
