package ir.tildaweb.atm.ui.signin.view_model;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.signin.model.SigninRequest;
import ir.tildaweb.atm.ui.signin.model.SigninResponse;
import ir.tildaweb.atm.ui.signin.repository.SigninRepository;

public class SigninViewModel {

    private SigninRepository repository;

    public SigninViewModel() {
        repository = new SigninRepository();
    }

    public Single<SigninResponse> requestProducts(SigninRequest request) {
        return repository.requestSignin(request);
    }
}
