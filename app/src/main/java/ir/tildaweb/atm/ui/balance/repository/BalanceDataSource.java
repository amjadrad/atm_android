package ir.tildaweb.atm.ui.balance.repository;


import io.reactivex.Single;
import ir.tildaweb.atm.ui.balance.model.BalanceRequest;
import ir.tildaweb.atm.ui.balance.model.BalanceResponse;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;

public interface BalanceDataSource {
    Single<BalanceResponse> requestAccountBalance(BalanceRequest request);
}
