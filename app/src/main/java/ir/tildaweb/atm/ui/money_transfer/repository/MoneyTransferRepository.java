package ir.tildaweb.atm.ui.money_transfer.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferRequest;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferResponse;
import ir.tildaweb.atm.ui.withdrawal.model.WithdrawalRequest;
import ir.tildaweb.atm.ui.withdrawal.model.WithdrawalResponse;

public class MoneyTransferRepository implements MoneyTransferDataSource {

    private ApiMoneyTransferDataSource apiDataSource;

    public MoneyTransferRepository() {
        this.apiDataSource = new ApiMoneyTransferDataSource();
    }

    @Override
    public Single<MoneyTransferResponse> requestMoneyTransfer(MoneyTransferRequest request) {
        return apiDataSource.requestMoneyTransfer(request);
    }
}
