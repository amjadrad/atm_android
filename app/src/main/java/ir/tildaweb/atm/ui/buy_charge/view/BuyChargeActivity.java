package ir.tildaweb.atm.ui.buy_charge.view;


import android.os.Bundle;
import android.view.View;

import androidx.core.content.ContextCompat;

import ir.tildaweb.atm.R;
import ir.tildaweb.atm.databinding.ActivityBuyChargeBinding;
import ir.tildaweb.atm.databinding.ActivityMoneyTransferBinding;
import ir.tildaweb.atm.dialog.DialogBottomSheetMessageConfirm;
import ir.tildaweb.atm.ui.base.BaseActivity;

public class BuyChargeActivity extends BaseActivity implements View.OnClickListener {

    private ActivityBuyChargeBinding binding;
    private Integer price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBuyChargeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("خرید شارژ");
        binding.btnConfirm.setOnClickListener(this);
        binding.tvSelect1Dollars.setOnClickListener(this);
        binding.tvSelect2Dollars.setOnClickListener(this);
        binding.tvSelect5Dollars.setOnClickListener(this);
        binding.tvSelect10Dollars.setOnClickListener(this);
        binding.tvSelect20Dollars.setOnClickListener(this);

    }

    private void showDialogConfirmPriceToDeposit(int price, String destinationCardNumber, String name) {

        DialogBottomSheetMessageConfirm dialogBottomSheetMessageConfirm = new DialogBottomSheetMessageConfirm();
        dialogBottomSheetMessageConfirm.setTitle("واریز وجه");
        dialogBottomSheetMessageConfirm.setDescription(String.format("آیا مطمئن هستید که مبلغ %d دلار به شماره کارت %s به نام %s واریز شود؟", price, destinationCardNumber, name));
        dialogBottomSheetMessageConfirm.setOnCancelClickListener(view -> dialogBottomSheetMessageConfirm.dismiss());
        dialogBottomSheetMessageConfirm.setOnConfirmClickListener(view -> {
            dialogBottomSheetMessageConfirm.dismiss();
        });
        dialogBottomSheetMessageConfirm.show(getSupportFragmentManager());

    }

    private void selectPrice(int price) {
        this.price = price;
        if (price == 1) {
            binding.tvSelect1Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_default));
            binding.tvSelect1Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));

            binding.tvSelect2Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect2Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.tvSelect5Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect5Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.tvSelect10Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect10Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.tvSelect20Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect20Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        } else if (price == 2) {
            binding.tvSelect2Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_default));
            binding.tvSelect2Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));

            binding.tvSelect1Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect1Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.tvSelect5Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect5Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.tvSelect10Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect10Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.tvSelect20Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect20Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        } else if (price == 5) {
            binding.tvSelect5Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_default));
            binding.tvSelect5Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));

            binding.tvSelect2Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect2Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.tvSelect1Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect1Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.tvSelect10Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect10Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.tvSelect20Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect20Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        } else if (price == 10) {
            binding.tvSelect10Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_default));
            binding.tvSelect10Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));

            binding.tvSelect2Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect2Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.tvSelect5Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect5Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.tvSelect1Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect1Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.tvSelect20Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect20Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        } else if (price == 20) {
            binding.tvSelect20Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_default));
            binding.tvSelect20Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));

            binding.tvSelect2Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect2Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.tvSelect5Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect5Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.tvSelect10Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect10Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            binding.tvSelect1Dollars.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_primary_pill_soft));
            binding.tvSelect1Dollars.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }
            case R.id.tvSelect1Dollars: {
                selectPrice(1);
                break;
            }
            case R.id.tvSelect2Dollars: {
                selectPrice(2);
                break;
            }
            case R.id.tvSelect5Dollars: {
                selectPrice(5);
                break;
            }
            case R.id.tvSelect10Dollars: {
                selectPrice(10);
                break;
            }
            case R.id.tvSelect20Dollars: {
                selectPrice(20);
                break;
            }
            case R.id.btnConfirm: {
                try {
                    String phone = binding.etPhone.getText().toString().trim();
                    if (phone.length() != 11 && !phone.startsWith("09")) {
                        toast("شماره تلفن را به درستی وارد کنید.");
                    } else if (price == null) {
                        toast("لطفا یکی از مبالغ را انتخاب کنید.");
                    }
//                    showDialogConfirmPriceToDeposit(price, destinationCardNumber, "...");
                } catch (Exception e) {
                    e.printStackTrace();
                    toast("مبلغ به درستی وارد نشده است.");
                }
                break;
            }
        }
    }
}