package ir.tildaweb.atm.ui.signup.view_model;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;
import ir.tildaweb.atm.ui.signup.repository.SignupRepository;

public class SignupViewModel {

    private SignupRepository repository;

    public SignupViewModel() {
        repository = new SignupRepository();
    }

    public Single<SignupResponse> requestProducts(SignupRequest request) {
        return repository.requestSignup(request);
    }
}
