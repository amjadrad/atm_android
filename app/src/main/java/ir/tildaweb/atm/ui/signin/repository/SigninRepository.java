package ir.tildaweb.atm.ui.signin.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.signin.model.SigninRequest;
import ir.tildaweb.atm.ui.signin.model.SigninResponse;

public class SigninRepository implements SigninDataSource {

    private ApiSigninDataSource apiDataSource;

    public SigninRepository() {
        this.apiDataSource = new ApiSigninDataSource();
    }


    @Override
    public Single<SigninResponse> requestSignin(SigninRequest request) {
        return apiDataSource.requestSignin(request);
    }
}
