package ir.tildaweb.atm.ui.signin.model;

import com.google.gson.annotations.SerializedName;

public class SigninRequest {

    @SerializedName("accountNumber")
    private String accountNumber;
    @SerializedName("password")
    private String password;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
