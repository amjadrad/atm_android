package ir.tildaweb.atm.ui.account_info.view;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import ir.tildaweb.atm.data.network.models.Account;
import ir.tildaweb.atm.databinding.ActivityAccountInfoBinding;
import ir.tildaweb.atm.dialog.DialogBottomSheetMessage;
import ir.tildaweb.atm.dialog.DialogBottomSheetMessageConfirm;
import ir.tildaweb.atm.ui.base.BaseActivity;
import ir.tildaweb.atm.ui.main.view.MainActivity;
import ir.tildaweb.atm.ui.splash.view.SplashActivity;

public class AccountInfoActivity extends BaseActivity implements View.OnClickListener {

    private ActivityAccountInfoBinding binding;
    private Account account;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAccountInfoBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.toolbar.tvToolbarTitle.setText("اطلاعات حساب");
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.btnConfirm.setOnClickListener(this);
        binding.linearShowPin.setOnClickListener(this);

        account = (Account) getIntent().getSerializableExtra("account");
        binding.tvCardNumber1.setText(String.format("%s", account.getCardNumber().substring(0, 4)));
        binding.tvCardNumber2.setText(String.format("%s", account.getCardNumber().substring(4, 8)));
        binding.tvCardNumber3.setText(String.format("%s", account.getCardNumber().substring(8, 12)));
        binding.tvCardNumber4.setText(String.format("%s", account.getCardNumber().substring(12, 16)));

        binding.tvAccountNumber.setText(String.format("شماره حساب: %s", account.getAccountNumber()));
        binding.tvExpireDate.setText(String.format("تاریخ انقضا: %s", account.getExpireDate().substring(0, 7)));
        binding.tvName.setText(String.format("%s", account.getName()));

    }

    @Override
    public void onBackPressed() {
        DialogBottomSheetMessage dialogBottomSheetMessageConfirm = new DialogBottomSheetMessage();
        dialogBottomSheetMessageConfirm.setTitle("هشدار");
        dialogBottomSheetMessageConfirm.setDescription("لطفا اطلاعات حساب خود را ذخیره کنید.");
        dialogBottomSheetMessageConfirm.setOnConfirmClickListener(view -> {
            dialogBottomSheetMessageConfirm.dismiss();
            super.onBackPressed();
        });
        dialogBottomSheetMessageConfirm.show(getSupportFragmentManager());

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == binding.btnConfirm.getId()) {
            startActivity(new Intent(AccountInfoActivity.this, MainActivity.class));
            finishAffinity();
        } else if (view.getId() == binding.toolbar.imageViewBack.getId()) {
            onBackPressed();
//            startActivity(new Intent(AccountInfoActivity.this, SplashActivity.class));
//            finish();
        } else if (view.getId() == binding.linearShowPin.getId()) {
            binding.tvPin.setText(String.format("%s", account.getPassword()));
        }
    }


}