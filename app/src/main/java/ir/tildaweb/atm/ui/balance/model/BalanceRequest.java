package ir.tildaweb.atm.ui.balance.model;

import com.google.gson.annotations.SerializedName;

public class BalanceRequest {

    @SerializedName("accountNumber")
    private String accountNumber;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
