package ir.tildaweb.atm.ui.withdrawal.view;


import android.os.Bundle;
import android.view.View;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;
import ir.tildaweb.atm.R;
import ir.tildaweb.atm.databinding.ActivityWithdrawalBinding;
import ir.tildaweb.atm.dialog.DialogBottomSheetMessage;
import ir.tildaweb.atm.dialog.DialogBottomSheetMessageConfirm;
import ir.tildaweb.atm.ui.base.BaseActivity;
import ir.tildaweb.atm.ui.deposit.model.DepositRequest;
import ir.tildaweb.atm.ui.deposit.model.DepositResponse;
import ir.tildaweb.atm.ui.withdrawal.model.WithdrawalRequest;
import ir.tildaweb.atm.ui.withdrawal.model.WithdrawalResponse;
import ir.tildaweb.atm.ui.withdrawal.view_model.WithdrawalViewModel;

public class WithdrawalActivity extends BaseActivity implements View.OnClickListener {

    private ActivityWithdrawalBinding binding;
    private WithdrawalViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityWithdrawalBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        viewModel = new WithdrawalViewModel();
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("برداشت وجه");
        binding.tvSelect10Dollars.setOnClickListener(this);
        binding.tvSelect20Dollars.setOnClickListener(this);
        binding.tvSelect50Dollars.setOnClickListener(this);
        binding.tvSelect100Dollars.setOnClickListener(this);
        binding.tvSelect200Dollars.setOnClickListener(this);
        binding.tvSelectCustomPrice.setOnClickListener(this);
        binding.btnConfirm.setOnClickListener(this);

    }

    private void showDialogConfirmPriceToWithdrawal(int price) {

        DialogBottomSheetMessageConfirm dialogBottomSheetMessageConfirm = new DialogBottomSheetMessageConfirm();
        dialogBottomSheetMessageConfirm.setTitle("برداشت وجه");
        dialogBottomSheetMessageConfirm.setDescription(String.format("آیا مطمئن هستید که مبلغ %d دلار از حساب شما برداشت شود؟", price));
        dialogBottomSheetMessageConfirm.setOnCancelClickListener(view -> dialogBottomSheetMessageConfirm.dismiss());
        dialogBottomSheetMessageConfirm.setOnConfirmClickListener(view -> {
            dialogBottomSheetMessageConfirm.dismiss();
            WithdrawalRequest request = new WithdrawalRequest();
            request.setAccountNumber(getAppPreferencesHelper().getAccountNumber());
            request.setAmount(price);
            requestWithdrawal(request);
        });
        dialogBottomSheetMessageConfirm.show(getSupportFragmentManager());

    }

    private void requestWithdrawal(WithdrawalRequest request) {
        showLoadingFullPage();
        viewModel.requestWithdrawal(request).subscribeOn(Schedulers.single()).observeOn(AndroidSchedulers.mainThread()).doFinally(new Action() {
            @Override
            public void run() {

            }
        }).subscribe(new SingleObserver<WithdrawalResponse>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onSuccess(WithdrawalResponse response) {
                dismissLoading();
                DialogBottomSheetMessage dialogBottomSheetMessage = new DialogBottomSheetMessage();
                dialogBottomSheetMessage.setTitle("برداشت وجه");
                dialogBottomSheetMessage.setDescription("لطفا پول خود را بردارید.");
                dialogBottomSheetMessage.setOnConfirmClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onBackPressed();
                    }
                });
                dialogBottomSheetMessage.show(getSupportFragmentManager());
            }

            @Override
            public void onError(Throwable e) {
                dismissLoading();
                e.printStackTrace();
                DialogBottomSheetMessage dialogBottomSheetMessage = new DialogBottomSheetMessage();
                dialogBottomSheetMessage.setTitle("برداشت وجه");
                dialogBottomSheetMessage.setDescription("امکان برداشت این مبلغ وجود ندارد. مبلغ کمتری را انتخاب کنید.");
                dialogBottomSheetMessage.setOnConfirmClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onBackPressed();
                    }
                });
                dialogBottomSheetMessage.show(getSupportFragmentManager());
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }
            case R.id.tvSelect10Dollars: {
                showDialogConfirmPriceToWithdrawal(10);
                break;
            }
            case R.id.tvSelect20Dollars: {
                showDialogConfirmPriceToWithdrawal(20);
                break;
            }
            case R.id.tvSelect50Dollars: {
                showDialogConfirmPriceToWithdrawal(50);
                break;
            }
            case R.id.tvSelect100Dollars: {
                showDialogConfirmPriceToWithdrawal(100);
                break;
            }
            case R.id.tvSelect200Dollars: {
                showDialogConfirmPriceToWithdrawal(200);
                break;
            }
            case R.id.tvSelectCustomPrice: {
                binding.etCustomPrice.setVisibility(View.VISIBLE);
                binding.btnConfirm.setVisibility(View.VISIBLE);
                break;
            }
            case R.id.btnConfirm: {
                try {
                    int price = Integer.parseInt(binding.etCustomPrice.getText().toString().trim());
                    showDialogConfirmPriceToWithdrawal(price);
                } catch (Exception e) {
                    e.printStackTrace();
                    toast("مبلغ به درستی وارد نشده است.");
                }
                break;
            }
        }
    }
}