package ir.tildaweb.atm.ui.signin.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.data.network.ApiProvider;
import ir.tildaweb.atm.data.network.ApiService;
import ir.tildaweb.atm.ui.signin.model.SigninRequest;
import ir.tildaweb.atm.ui.signin.model.SigninResponse;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;

public class SigninApiService implements SigninDataSource {

    private ApiService apiService;

    public SigninApiService() {
        this.apiService = ApiProvider.apiProvider();
    }

    @Override
    public Single<SigninResponse> requestSignin(SigninRequest request) {
        return apiService.requestSignin(request.getAccountNumber(), request.getPassword());
    }
}
