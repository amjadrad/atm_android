package ir.tildaweb.atm.ui.bill_payment.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.bill_payment.model.BillPaymentRequest;
import ir.tildaweb.atm.ui.bill_payment.model.BillPaymentResponse;
import ir.tildaweb.atm.ui.buy_charge.model.BuyChargeRequest;
import ir.tildaweb.atm.ui.buy_charge.model.BuyChargeResponse;

public class ApiBillPaymentDataSource implements BillPaymentDataSource {

    private BillPaymentApiService apiService;

    public ApiBillPaymentDataSource() {
        this.apiService = new BillPaymentApiService();
    }

    @Override
    public Single<BillPaymentResponse> requestBillPayment(BillPaymentRequest request) {
        return apiService.requestBillPayment(request);
    }
}
