package ir.tildaweb.atm.ui.deposit.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.deposit.model.DepositRequest;
import ir.tildaweb.atm.ui.deposit.model.DepositResponse;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferRequest;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferResponse;

public class DepositRepository implements DepositDataSource {

    private ApiDepositDataSource apiDataSource;

    public DepositRepository() {
        this.apiDataSource = new ApiDepositDataSource();
    }

    @Override
    public Single<DepositResponse> requestDeposit(DepositRequest request) {
        return apiDataSource.requestDeposit(request);
    }
}
