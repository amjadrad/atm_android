package ir.tildaweb.atm.ui.withdrawal.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;
import ir.tildaweb.atm.ui.withdrawal.model.WithdrawalRequest;
import ir.tildaweb.atm.ui.withdrawal.model.WithdrawalResponse;

public class ApiWithdrawalDataSource implements WithdrawalDataSource {

    private WithdrawalApiService apiService;

    public ApiWithdrawalDataSource(){
        this.apiService = new WithdrawalApiService();
    }


    @Override
    public Single<WithdrawalResponse> requestWithdrawal(WithdrawalRequest request) {
        return apiService.requestWithdrawal(request);
    }
}
