package ir.tildaweb.atm.ui.signup.model;

import com.google.gson.annotations.SerializedName;

import ir.tildaweb.atm.data.network.models.Account;

public class SignupResponse  {

    @SerializedName("result")
    private Account account;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
