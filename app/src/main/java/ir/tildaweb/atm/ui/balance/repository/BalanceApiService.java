package ir.tildaweb.atm.ui.balance.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.data.network.ApiProvider;
import ir.tildaweb.atm.data.network.ApiService;
import ir.tildaweb.atm.ui.balance.model.BalanceRequest;
import ir.tildaweb.atm.ui.balance.model.BalanceResponse;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;

public class BalanceApiService implements BalanceDataSource {

    private ApiService apiService;

    public BalanceApiService() {
        this.apiService = ApiProvider.apiProvider();
    }


    @Override
    public Single<BalanceResponse> requestAccountBalance(BalanceRequest request) {
        return apiService.requestAccountBalance(request.getAccountNumber());
    }
}
