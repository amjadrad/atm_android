package ir.tildaweb.atm.ui.signin.repository;


import io.reactivex.Single;
import ir.tildaweb.atm.ui.signin.model.SigninRequest;
import ir.tildaweb.atm.ui.signin.model.SigninResponse;

public interface SigninDataSource {
    Single<SigninResponse> requestSignin(SigninRequest request);
}
