package ir.tildaweb.atm.ui.money_transfer.view;


import android.os.Bundle;
import android.view.View;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;
import ir.tildaweb.atm.R;
import ir.tildaweb.atm.databinding.ActivityMoneyTransferBinding;
import ir.tildaweb.atm.dialog.DialogBottomSheetMessage;
import ir.tildaweb.atm.dialog.DialogBottomSheetMessageConfirm;
import ir.tildaweb.atm.ui.base.BaseActivity;
import ir.tildaweb.atm.ui.deposit.model.DepositRequest;
import ir.tildaweb.atm.ui.deposit.model.DepositResponse;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferRequest;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferResponse;
import ir.tildaweb.atm.ui.money_transfer.view_model.MoneyTransferViewModel;

public class MoneyTransferActivity extends BaseActivity implements View.OnClickListener {

    private ActivityMoneyTransferBinding binding;
    private MoneyTransferViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMoneyTransferBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        viewModel = new MoneyTransferViewModel();
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("انتقال وجه");
        binding.btnConfirm.setOnClickListener(this);

    }

    private void showDialogConfirmPriceToDeposit(int price, String destinationCardNumber) {

        DialogBottomSheetMessageConfirm dialogBottomSheetMessageConfirm = new DialogBottomSheetMessageConfirm();
        dialogBottomSheetMessageConfirm.setTitle("واریز وجه");
        dialogBottomSheetMessageConfirm.setDescription(String.format("آیا مطمئن هستید که مبلغ %d دلار به شماره کارت %s واریز شود؟", price, destinationCardNumber));
        dialogBottomSheetMessageConfirm.setOnCancelClickListener(view -> dialogBottomSheetMessageConfirm.dismiss());
        dialogBottomSheetMessageConfirm.setOnConfirmClickListener(view -> {
            dialogBottomSheetMessageConfirm.dismiss();
            MoneyTransferRequest request = new MoneyTransferRequest();
            request.setAmount(price);
            request.setAccountNumber(getAppPreferencesHelper().getAccountNumber());
            request.setCardNumber(destinationCardNumber);
            requestTransfer(request);
        });
        dialogBottomSheetMessageConfirm.show(getSupportFragmentManager());

    }


    private void requestTransfer(MoneyTransferRequest request) {
        showLoadingFullPage();
        viewModel.requestMoneyTransfer(request).subscribeOn(Schedulers.single()).observeOn(AndroidSchedulers.mainThread()).doFinally(new Action() {
            @Override
            public void run() {

            }
        }).subscribe(new SingleObserver<MoneyTransferResponse>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onSuccess(MoneyTransferResponse response) {
                dismissLoading();
                DialogBottomSheetMessage dialogBottomSheetMessage = new DialogBottomSheetMessage();
                dialogBottomSheetMessage.setTitle("انتقال وجه");
                dialogBottomSheetMessage.setDescription("انتقال وجه با موفقیت انجام شد.");
                dialogBottomSheetMessage.setOnConfirmClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onBackPressed();
                    }
                });
                dialogBottomSheetMessage.show(getSupportFragmentManager());
            }

            @Override
            public void onError(Throwable e) {
                dismissLoading();
                e.printStackTrace();
                toast("مشکلی رخ داده است. مجددا امتحان کنید.");
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }

            case R.id.btnConfirm: {
                try {
                    String destinationCardNumber = binding.etDestinationCardNumber.getText().toString().trim();
                    int price = Integer.parseInt(binding.etPrice.getText().toString().trim());
                    showDialogConfirmPriceToDeposit(price, destinationCardNumber);
                } catch (Exception e) {
                    e.printStackTrace();
                    toast("مبلغ به درستی وارد نشده است.");
                }
                break;
            }
        }
    }
}