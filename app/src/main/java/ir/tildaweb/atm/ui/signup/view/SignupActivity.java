package ir.tildaweb.atm.ui.signup.view;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;
import ir.tildaweb.atm.databinding.ActivitySignupBinding;
import ir.tildaweb.atm.ui.account_info.view.AccountInfoActivity;
import ir.tildaweb.atm.ui.base.BaseActivity;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;
import ir.tildaweb.atm.ui.signup.view_model.SignupViewModel;
import ir.tildaweb.atm.ui.splash.view.SplashActivity;

public class SignupActivity extends BaseActivity implements View.OnClickListener {

    private ActivitySignupBinding binding;
    private SignupViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySignupBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        viewModel = new SignupViewModel();
        binding.toolbar.tvToolbarTitle.setText("افتتاح حساب");
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.btnConfirm.setOnClickListener(this);
    }

    private void requestSignup(SignupRequest request) {
        showLoadingFullPage();
        viewModel.requestProducts(request).subscribeOn(Schedulers.single()).observeOn(AndroidSchedulers.mainThread()).doFinally(new Action() {
            @Override
            public void run() {

            }
        }).subscribe(new SingleObserver<SignupResponse>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onSuccess(SignupResponse response) {
                dismissLoading();
                getAppPreferencesHelper().setLoginPref(true);
                getAppPreferencesHelper().setAccountNumber(response.getAccount().getAccountNumber());
                Intent intent = new Intent(SignupActivity.this, AccountInfoActivity.class);
                intent.putExtra("account", response.getAccount());
//                intent.putExtra("name", response.getAccount().getName());
//                intent.putExtra("card_number", response.getAccount().getCardNumber());
//                intent.putExtra("account_number", response.getAccount().getAccountNumber());
//                intent.putExtra("expire_date", response.getAccount().getExpireDate());
//                intent.putExtra("password", response.getAccount().getPassword());
                startActivity(intent);
                finish();
            }

            @Override
            public void onError(Throwable e) {
                dismissLoading();
                toast("مشکلی رخ داده است. مجددا امتحان کنید.");
            }
        });
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == binding.btnConfirm.getId()) {
            String name = binding.etName.getText().toString().trim();
            String nationalCode = binding.etNationalCode.getText().toString().trim();
            String phone = binding.etPhone.getText().toString().trim();

            if (name.length() < 3) {
                toast("لطفا نام و نام خانوادگی را وارد کنید.");
            } else if (nationalCode.length() != 10) {
                toast("لطفا کدملی را وارد کنید. کد ملی بایستی 10 رقم باشد.");
            } else if (phone.length() < 3) {
                toast("لطفا شماره همراه را وارد کنید.");
            } else {
                //Request
                SignupRequest request = new SignupRequest();
                request.setName(name);
                request.setNationalCode(nationalCode);
                request.setPhone(phone);
                requestSignup(request);
            }

        } else if (view.getId() == binding.toolbar.imageViewBack.getId()) {
            onBackPressed();
//            startActivity(new Intent(SignupActivity.this, SplashActivity.class));
//            finish();
        }
    }
}