package ir.tildaweb.atm.ui.bill_payment.view;


import android.os.Bundle;
import android.view.View;

import ir.tildaweb.atm.R;
import ir.tildaweb.atm.databinding.ActivityBillPaymentBinding;
import ir.tildaweb.atm.dialog.DialogBottomSheetMessageConfirm;
import ir.tildaweb.atm.ui.base.BaseActivity;

public class BillPaymentActivity extends BaseActivity implements View.OnClickListener {

    private ActivityBillPaymentBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBillPaymentBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("پرداخت قبض");
        binding.btnConfirm.setOnClickListener(this);

    }

    private void showDialogConfirmPriceToDeposit(int price, String destinationCardNumber, String name) {

        DialogBottomSheetMessageConfirm dialogBottomSheetMessageConfirm = new DialogBottomSheetMessageConfirm();
        dialogBottomSheetMessageConfirm.setTitle("واریز وجه");
        dialogBottomSheetMessageConfirm.setDescription(String.format("آیا مطمئن هستید که مبلغ %d دلار به شماره کارت %s به نام %s واریز شود؟", price, destinationCardNumber, name));
        dialogBottomSheetMessageConfirm.setOnCancelClickListener(view -> dialogBottomSheetMessageConfirm.dismiss());
        dialogBottomSheetMessageConfirm.setOnConfirmClickListener(view -> {
            dialogBottomSheetMessageConfirm.dismiss();
        });
        dialogBottomSheetMessageConfirm.show(getSupportFragmentManager());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }

            case R.id.btnConfirm: {
                try {
                    String destinationCardNumber = binding.etBillCode.getText().toString().trim();
                    int price = Integer.parseInt(binding.etPaymentCode.getText().toString().trim());
                    //Check account user name...
                    showDialogConfirmPriceToDeposit(price, destinationCardNumber, "...");
                } catch (Exception e) {
                    e.printStackTrace();
                    toast("مبلغ به درستی وارد نشده است.");
                }
                break;
            }
        }
    }
}