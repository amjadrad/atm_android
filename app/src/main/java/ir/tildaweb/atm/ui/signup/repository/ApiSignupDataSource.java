package ir.tildaweb.atm.ui.signup.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;

public class ApiSignupDataSource implements SignupDataSource {

    private SignupApiService apiService;

    public ApiSignupDataSource(){
        this.apiService = new SignupApiService();
    }

    @Override
    public Single<SignupResponse> requestSignup(SignupRequest request) {
        return apiService.requestSignup(request);
    }
}
