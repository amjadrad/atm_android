package ir.tildaweb.atm.ui.buy_charge.view_model;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.buy_charge.model.BuyChargeRequest;
import ir.tildaweb.atm.ui.buy_charge.model.BuyChargeResponse;
import ir.tildaweb.atm.ui.buy_charge.repository.BuyChargeRepository;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;
import ir.tildaweb.atm.ui.signup.repository.SignupRepository;

public class BuyChargeViewModel {

    private BuyChargeRepository repository;

    public BuyChargeViewModel() {
        repository = new BuyChargeRepository();
    }

    public Single<BuyChargeResponse> requestBuyCharge(BuyChargeRequest request) {
        return repository.requestBuyCharge(request);
    }
}
