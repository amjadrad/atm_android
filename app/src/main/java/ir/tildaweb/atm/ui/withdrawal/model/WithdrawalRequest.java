package ir.tildaweb.atm.ui.withdrawal.model;

import com.google.gson.annotations.SerializedName;

public class WithdrawalRequest {
    @SerializedName("accountNumber")
    private String accountNumber;
    @SerializedName("amount")
    private Integer amount;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
