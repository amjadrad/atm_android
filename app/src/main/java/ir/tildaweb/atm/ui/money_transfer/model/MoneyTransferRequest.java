package ir.tildaweb.atm.ui.money_transfer.model;

import com.google.gson.annotations.SerializedName;

public class MoneyTransferRequest {

    @SerializedName("accountNumber")
    private String accountNumber;
    @SerializedName("cardNumber")
    private String cardNumber;
    @SerializedName("amount")
    private Integer amount;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
