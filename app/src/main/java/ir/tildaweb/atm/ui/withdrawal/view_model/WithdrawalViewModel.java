package ir.tildaweb.atm.ui.withdrawal.view_model;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;
import ir.tildaweb.atm.ui.signup.repository.SignupRepository;
import ir.tildaweb.atm.ui.withdrawal.model.WithdrawalRequest;
import ir.tildaweb.atm.ui.withdrawal.model.WithdrawalResponse;
import ir.tildaweb.atm.ui.withdrawal.repository.WithdrawalRepository;

public class WithdrawalViewModel {

    private WithdrawalRepository repository;

    public WithdrawalViewModel() {
        repository = new WithdrawalRepository();
    }

    public Single<WithdrawalResponse> requestWithdrawal(WithdrawalRequest request) {
        return repository.requestWithdrawal(request);
    }
}
