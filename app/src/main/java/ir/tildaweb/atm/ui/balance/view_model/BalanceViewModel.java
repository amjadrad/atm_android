package ir.tildaweb.atm.ui.balance.view_model;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.balance.model.BalanceRequest;
import ir.tildaweb.atm.ui.balance.model.BalanceResponse;
import ir.tildaweb.atm.ui.balance.repository.BalanceRepository;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;
import ir.tildaweb.atm.ui.signup.repository.SignupRepository;

public class BalanceViewModel {

    private BalanceRepository repository;

    public BalanceViewModel() {
        repository = new BalanceRepository();
    }

    public Single<BalanceResponse> requestAccountBalance(BalanceRequest request) {
        return repository.requestAccountBalance(request);
    }
}
