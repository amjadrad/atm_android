package ir.tildaweb.atm.ui.withdrawal.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.withdrawal.model.WithdrawalRequest;
import ir.tildaweb.atm.ui.withdrawal.model.WithdrawalResponse;

public class WithdrawalRepository implements WithdrawalDataSource {

    private ApiWithdrawalDataSource apiDataSource;

    public WithdrawalRepository() {
        this.apiDataSource = new ApiWithdrawalDataSource();
    }


    @Override
    public Single<WithdrawalResponse> requestWithdrawal(WithdrawalRequest request) {
        return apiDataSource.requestWithdrawal(request);
    }
}
