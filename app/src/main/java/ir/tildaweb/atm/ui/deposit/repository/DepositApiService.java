package ir.tildaweb.atm.ui.deposit.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.data.network.ApiProvider;
import ir.tildaweb.atm.data.network.ApiService;
import ir.tildaweb.atm.ui.deposit.model.DepositRequest;
import ir.tildaweb.atm.ui.deposit.model.DepositResponse;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferRequest;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferResponse;

public class DepositApiService implements DepositDataSource {

    private ApiService apiService;

    public DepositApiService() {
        this.apiService = ApiProvider.apiProvider();
    }


    @Override
    public Single<DepositResponse> requestDeposit(DepositRequest request) {
        return apiService.requestDeposit(request.getAccountNumber(),request.getAmount());
    }
}
