package ir.tildaweb.atm.ui.buy_charge.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.buy_charge.model.BuyChargeRequest;
import ir.tildaweb.atm.ui.buy_charge.model.BuyChargeResponse;

public class BuyChargeRepository implements BuyChargeDataSource {

    private ApiBuyChargeDataSource apiDataSource;

    public BuyChargeRepository() {
        this.apiDataSource = new ApiBuyChargeDataSource();
    }

    @Override
    public Single<BuyChargeResponse> requestBuyCharge(BuyChargeRequest request) {
        return apiDataSource.requestBuyCharge(request);
    }
}
