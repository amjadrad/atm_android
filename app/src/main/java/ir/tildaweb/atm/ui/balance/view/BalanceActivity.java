package ir.tildaweb.atm.ui.balance.view;


import android.os.Bundle;
import android.view.View;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;
import ir.tildaweb.atm.R;
import ir.tildaweb.atm.databinding.ActivityBalanceBinding;
import ir.tildaweb.atm.ui.balance.model.BalanceRequest;
import ir.tildaweb.atm.ui.balance.model.BalanceResponse;
import ir.tildaweb.atm.ui.balance.view_model.BalanceViewModel;
import ir.tildaweb.atm.ui.base.BaseActivity;


public class BalanceActivity extends BaseActivity implements View.OnClickListener {

    private ActivityBalanceBinding binding;
    private BalanceViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBalanceBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        viewModel = new BalanceViewModel();
        binding.toolbar.imageViewBack.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("موجودی حساب");
        BalanceRequest request = new BalanceRequest();
        request.setAccountNumber(getAppPreferencesHelper().getAccountNumber());
        requestBalance(request);

    }

    private void requestBalance(BalanceRequest request) {
        showLoadingFullPage();
        viewModel.requestAccountBalance(request).subscribeOn(Schedulers.single()).observeOn(AndroidSchedulers.mainThread()).doFinally(new Action() {
            @Override
            public void run() {

            }
        }).subscribe(new SingleObserver<BalanceResponse>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onSuccess(BalanceResponse response) {
                dismissLoading();
                binding.tvBalanceAvailable.setText(String.format("قابل برداشت: %s", response.getResult().getAvailableBalance()));
                binding.tvBalance.setText(String.format("%s", response.getResult().getBalance()));
            }

            @Override
            public void onError(Throwable e) {
                dismissLoading();
                e.printStackTrace();
                toast("مشکلی رخ داده است. مجددا امتحان کنید.");
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack: {
                onBackPressed();
                break;
            }
        }
    }
}