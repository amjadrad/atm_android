package ir.tildaweb.atm.ui.balance.repository;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.balance.model.BalanceRequest;
import ir.tildaweb.atm.ui.balance.model.BalanceResponse;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;

public class BalanceRepository implements BalanceDataSource {

    private ApiBalanceDataSource apiDataSource;

    public BalanceRepository() {
        this.apiDataSource = new ApiBalanceDataSource();
    }


    @Override
    public Single<BalanceResponse> requestAccountBalance(BalanceRequest request) {
        return apiDataSource.requestAccountBalance(request);
    }
}
