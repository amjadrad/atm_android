package ir.tildaweb.atm.ui.balance.model;

import android.content.Intent;

import com.google.gson.annotations.SerializedName;

public class BalanceResponse {

    @SerializedName("result")
    private RequestResult result;


    public class RequestResult {
        @SerializedName("balance")
        private Integer balance;
        @SerializedName("availableBalance")
        private Integer availableBalance;

        public Integer getBalance() {
            return balance;
        }

        public void setBalance(Integer balance) {
            this.balance = balance;
        }

        public Integer getAvailableBalance() {
            return availableBalance;
        }

        public void setAvailableBalance(Integer availableBalance) {
            this.availableBalance = availableBalance;
        }
    }


    public RequestResult getResult() {
        return result;
    }

    public void setResult(RequestResult result) {
        this.result = result;
    }
}
