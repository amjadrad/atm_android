package ir.tildaweb.atm.ui.money_transfer.repository;


import io.reactivex.Single;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferRequest;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferResponse;
import ir.tildaweb.atm.ui.withdrawal.model.WithdrawalRequest;
import ir.tildaweb.atm.ui.withdrawal.model.WithdrawalResponse;

public interface MoneyTransferDataSource {

    Single<MoneyTransferResponse> requestMoneyTransfer(MoneyTransferRequest request);
}