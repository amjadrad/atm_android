package ir.tildaweb.atm.ui.bill_payment.view_model;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.bill_payment.model.BillPaymentRequest;
import ir.tildaweb.atm.ui.bill_payment.model.BillPaymentResponse;
import ir.tildaweb.atm.ui.bill_payment.repository.BillPaymentRepository;
import ir.tildaweb.atm.ui.buy_charge.model.BuyChargeRequest;
import ir.tildaweb.atm.ui.buy_charge.model.BuyChargeResponse;
import ir.tildaweb.atm.ui.buy_charge.repository.BuyChargeRepository;

public class BillPaymentViewModel {

    private BillPaymentRepository repository;

    public BillPaymentViewModel() {
        repository = new BillPaymentRepository();
    }

    public Single<BillPaymentResponse> requestBillPayment(BillPaymentRequest request) {
        return repository.requestBillPayment(request);
    }
}
