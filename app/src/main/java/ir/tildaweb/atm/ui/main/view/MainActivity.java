package ir.tildaweb.atm.ui.main.view;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.core.content.res.ResourcesCompat;

import ir.tildaweb.atm.R;
import ir.tildaweb.atm.databinding.ActivityAccountInfoBinding;
import ir.tildaweb.atm.databinding.ActivityMainBinding;
import ir.tildaweb.atm.dialog.DialogBottomSheetMessage;
import ir.tildaweb.atm.dialog.DialogBottomSheetMessageConfirm;
import ir.tildaweb.atm.ui.balance.view.BalanceActivity;
import ir.tildaweb.atm.ui.base.BaseActivity;
import ir.tildaweb.atm.ui.bill_payment.view.BillPaymentActivity;
import ir.tildaweb.atm.ui.buy_charge.view.BuyChargeActivity;
import ir.tildaweb.atm.ui.deposit.view.DepositActivity;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferRequest;
import ir.tildaweb.atm.ui.money_transfer.view.MoneyTransferActivity;
import ir.tildaweb.atm.ui.splash.view.SplashActivity;
import ir.tildaweb.atm.ui.withdrawal.view.WithdrawalActivity;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.tvTitle.setTypeface(ResourcesCompat.getFont(this, R.font.pinar));
        binding.cardBalance.setOnClickListener(this);
        binding.cardWithdrawal.setOnClickListener(this);
        binding.cardDeposit.setOnClickListener(this);
        binding.cardMoneyTransfer.setOnClickListener(this);
        binding.cardBillPayment.setOnClickListener(this);
        binding.cardBuyCharge.setOnClickListener(this);
        binding.cardLogout.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        binding.cardLogout.callOnClick();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cardBalance: {
                startActivity(new Intent(MainActivity.this, BalanceActivity.class));
                break;
            }
            case R.id.cardWithdrawal: {
                startActivity(new Intent(MainActivity.this, WithdrawalActivity.class));
                break;
            }
            case R.id.cardDeposit: {
                startActivity(new Intent(MainActivity.this, DepositActivity.class));
                break;
            }
            case R.id.cardMoneyTransfer: {
                startActivity(new Intent(MainActivity.this, MoneyTransferActivity.class));
                break;
            }
            case R.id.cardBillPayment: {
                startActivity(new Intent(MainActivity.this, BillPaymentActivity.class));
                break;
            }
            case R.id.cardBuyCharge: {
                startActivity(new Intent(MainActivity.this, BuyChargeActivity.class));
                break;
            }
            case R.id.cardLogout: {
                DialogBottomSheetMessageConfirm dialogBottomSheetMessage = new DialogBottomSheetMessageConfirm();
                dialogBottomSheetMessage.setTitle("دریافت کارت");
                dialogBottomSheetMessage.setDescription("اگر مایل به دریافت کارت هستید 'تایید' و در صورت انجام عملیات دیگر 'بیخیال' را انتخاب کنید.");
                dialogBottomSheetMessage.setOnCancelClickListener(view2 -> {
                    dialogBottomSheetMessage.dismiss();
                });
                dialogBottomSheetMessage.setOnConfirmClickListener(view2 -> {
                    dialogBottomSheetMessage.dismiss();
                    getAppPreferencesHelper().setLoginPref(false);
                    getAppPreferencesHelper().setAccountNumber("");
                    startActivity(new Intent(MainActivity.this, SplashActivity.class));
                });
                dialogBottomSheetMessage.show(getSupportFragmentManager());
                break;
            }
        }
    }
}