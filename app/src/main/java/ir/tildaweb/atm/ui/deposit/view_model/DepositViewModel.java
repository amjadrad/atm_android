package ir.tildaweb.atm.ui.deposit.view_model;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.deposit.model.DepositRequest;
import ir.tildaweb.atm.ui.deposit.model.DepositResponse;
import ir.tildaweb.atm.ui.deposit.repository.DepositRepository;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;
import ir.tildaweb.atm.ui.signup.repository.SignupRepository;

public class DepositViewModel {

    private DepositRepository repository;

    public DepositViewModel() {
        repository = new DepositRepository();
    }

    public Single<DepositResponse> requestDeposit(DepositRequest request) {
        return repository.requestDeposit(request);
    }
}
