package ir.tildaweb.atm.data.network;

import ir.tildaweb.atm.BuildConfig;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static Retrofit retrofit = null;

    private static final String BASE_URL = BuildConfig.BASE_URL;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(HttpClient.getUnsafeOkHttpClient().build())
                    .baseUrl(BASE_URL)
                    .build();
        }
        return retrofit;
    }

}