package ir.tildaweb.atm.data.network.models;

import com.google.gson.annotations.SerializedName;

public class CashDispenser {

    @SerializedName("balance")
    private Integer balance;

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }
}
