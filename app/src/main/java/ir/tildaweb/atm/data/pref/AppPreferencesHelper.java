package ir.tildaweb.atm.data.pref;


import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferencesHelper implements PreferencesPresenter {


    private final String prefFileName = "main_pref";
    private final SharedPreferences mPrefs;

    public AppPreferencesHelper(Context context) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }


    @Override
    public void setLoginPref(boolean loginPref) {
        mPrefs.edit().putBoolean(PrefModel.PREF_LOGIN, loginPref).apply();
    }

    @Override
    public boolean getLoginPref() {
        return mPrefs.getBoolean(PrefModel.PREF_LOGIN, false);
    }

    @Override
    public void setAccountNumber(String accountNumber) {
        mPrefs.edit().putString(PrefModel.PREF_ACCOUNT_NUMBER, accountNumber).apply();
    }

    @Override
    public String getAccountNumber() {
        return mPrefs.getString(PrefModel.PREF_ACCOUNT_NUMBER, "");
    }


}