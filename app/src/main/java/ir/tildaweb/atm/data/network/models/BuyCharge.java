package ir.tildaweb.atm.data.network.models;

import com.google.gson.annotations.SerializedName;

public class BuyCharge extends Transaction {

    @SerializedName("id")
    private Integer id;
    @SerializedName("phone")
    private String phone;
    @SerializedName("transactionId")
    private Integer transactionId;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }
}
