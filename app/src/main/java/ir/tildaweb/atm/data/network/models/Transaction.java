package ir.tildaweb.atm.data.network.models;

import com.google.gson.annotations.SerializedName;

public class Transaction {

    @SerializedName("id")
    private Integer id;
    @SerializedName("accountId")
    private Integer accountId;
    @SerializedName("amount")
    private String amount;
    @SerializedName("type")
    private String type;
    @SerializedName("date")
    private String date;
    @SerializedName("status")
    private String status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
