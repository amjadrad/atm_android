package ir.tildaweb.atm.data.pref;

public interface PreferencesPresenter {


    void setLoginPref(boolean loginPref);

    boolean getLoginPref();

    void setAccountNumber(String accountNumber);

    String getAccountNumber();


}

