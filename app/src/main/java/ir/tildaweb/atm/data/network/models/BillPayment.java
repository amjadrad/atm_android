package ir.tildaweb.atm.data.network.models;

import com.google.gson.annotations.SerializedName;

public class BillPayment extends Transaction {

    @SerializedName("id")
    private Integer id;
    @SerializedName("billCode")
    private String billCode;
    @SerializedName("paymentCode")
    private String paymentCode;
    @SerializedName("transactionId")
    private Integer transactionId;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }
}
