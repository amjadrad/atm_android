package ir.tildaweb.atm.data.network.models;

import com.google.gson.annotations.SerializedName;

public class MoneyTransfer extends Transaction {

    @SerializedName("id")
    private Integer id;
    @SerializedName("receiverAccountId")
    private Integer receiverAccountId;
    @SerializedName("transactionId")
    private Integer transactionId;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getReceiverAccountId() {
        return receiverAccountId;
    }

    public void setReceiverAccountId(Integer receiverAccountId) {
        this.receiverAccountId = receiverAccountId;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }
}
