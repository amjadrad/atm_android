package ir.tildaweb.atm.data.network.models;

import com.google.gson.annotations.SerializedName;

public class Deposit extends Transaction {

    @SerializedName("id")
    private Integer id;
    @SerializedName("transactionsId")
    private Integer transactionsId;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTransactionsId() {
        return transactionsId;
    }

    public void setTransactionsId(Integer transactionsId) {
        this.transactionsId = transactionsId;
    }
}