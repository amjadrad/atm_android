package ir.tildaweb.atm.data.network;

import io.reactivex.Single;
import ir.tildaweb.atm.ui.balance.model.BalanceResponse;
import ir.tildaweb.atm.ui.bill_payment.model.BillPaymentRequest;
import ir.tildaweb.atm.ui.bill_payment.model.BillPaymentResponse;
import ir.tildaweb.atm.ui.buy_charge.model.BuyChargeRequest;
import ir.tildaweb.atm.ui.buy_charge.model.BuyChargeResponse;
import ir.tildaweb.atm.ui.deposit.model.DepositRequest;
import ir.tildaweb.atm.ui.deposit.model.DepositResponse;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferRequest;
import ir.tildaweb.atm.ui.money_transfer.model.MoneyTransferResponse;
import ir.tildaweb.atm.ui.signin.model.SigninRequest;
import ir.tildaweb.atm.ui.signin.model.SigninResponse;
import ir.tildaweb.atm.ui.signup.model.SignupRequest;
import ir.tildaweb.atm.ui.signup.model.SignupResponse;
import ir.tildaweb.atm.ui.withdrawal.model.WithdrawalRequest;
import ir.tildaweb.atm.ui.withdrawal.model.WithdrawalResponse;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {

    @GET("account/singin")
    Single<SigninResponse> requestSignin(@Query("accountNumber") String accountNumber, @Query("password") String password);

    @POST("account/signup")
    Single<SignupResponse> requestSignup(@Body SignupRequest request);

    @GET("account/balance")
    Single<BalanceResponse> requestAccountBalance(@Query("accountNumber") String accountNumber);

    @POST("transaction/withdrawal")
    Single<WithdrawalResponse> requestWithdrawal(@Query("accountNumber") String accountNumber, @Query("amount") Integer amount);

    @POST("transaction/deposit")
    Single<DepositResponse> requestDeposit(@Query("accountNumber") String accountNumber, @Query("amount") Integer amount);

    @POST("transaction/transfer")
    Single<MoneyTransferResponse> requestTransfer(@Query("accountNumber") String accountNumber, @Query("cardNumber") String cardNumber, @Query("amount") Integer amount);

    @POST("transaction/billPayment")
    Single<BillPaymentResponse> requestBillPayment(@Body BillPaymentRequest request);

    @POST("transaction/charge")
    Single<BuyChargeResponse> requestCharge(@Body BuyChargeRequest request);


}
