package ir.tildaweb.atm.dialog;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import ir.tildaweb.atm.R;
import ir.tildaweb.atm.databinding.DialogBottomSheetMessageBinding;

public class DialogBottomSheetMessage extends BottomSheetDialogFragment {

    private String TAG = this.getClass().getName();
    private DialogBottomSheetMessageBinding binding;
    private String title;
    private String description;
    private String confirmText = "تایید";
    private boolean isDanger;
    private View.OnClickListener onConfirm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.ModalBottomSheetDialog);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DialogBottomSheetMessageBinding.inflate(inflater, container, false);
        binding.btnConfirm.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_primary_pill_default));
        if (isDanger) {
            binding.tvTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.colorDanger));
            binding.btnConfirm.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_danger_pill_default));
        }
        if (confirmText != null) {
            binding.btnConfirm.setText(confirmText);
        }
        binding.tvTitle.setText(title);
        binding.tvDescription.setText(description);
        if (onConfirm != null) {
            binding.btnConfirm.setOnClickListener(onConfirm);
        }

        return binding.getRoot();
    }

    public DialogBottomSheetMessage setTitle(String title) {
        this.title = title;
        return this;
    }

    public DialogBottomSheetMessage setDescription(String description) {
        this.description = description;
        return this;
    }

    public DialogBottomSheetMessage setDanger() {
        this.isDanger = true;
        return this;
    }

    public DialogBottomSheetMessage setOnConfirmClickListener(View.OnClickListener onClickListener) {
        this.onConfirm = onClickListener;
        return this;
    }

    public DialogBottomSheetMessage setConfirmText(String str) {
        this.confirmText = str;
        return this;
    }

    public void show(FragmentManager fragmentManager) {
        this.show(fragmentManager, null);
    }

}