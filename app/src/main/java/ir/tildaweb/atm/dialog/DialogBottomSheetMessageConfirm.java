package ir.tildaweb.atm.dialog;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import ir.tildaweb.atm.R;
import ir.tildaweb.atm.databinding.DialogBottomSheetMessageConfirmBinding;

public class DialogBottomSheetMessageConfirm extends BottomSheetDialogFragment {

    private String TAG = this.getClass().getName();
    private DialogBottomSheetMessageConfirmBinding binding;
    private String title;
    private String description;
    private String confirmText = "تایید";
    private String cancelText = "بیخیال";
    private boolean isDanger;
    private View.OnClickListener onCancel, onConfirm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.ModalBottomSheetDialog);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DialogBottomSheetMessageConfirmBinding.inflate(inflater, container, false);
        binding.btnConfirm.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_primary_pill_default));
        if (isDanger) {
            binding.tvTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.colorDanger));
            binding.btnConfirm.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_danger_pill_default));
        }
        if (confirmText != null) {
            binding.btnConfirm.setText(confirmText);
        }
        if (cancelText != null) {
            binding.btnCancel.setText(cancelText);
        }
        binding.tvTitle.setText(title);
        binding.tvDescription.setText(description);
        if (onCancel != null) {
            binding.btnCancel.setOnClickListener(onCancel);
        } else {
            binding.btnCancel.setOnClickListener(view -> dismiss());
        }
        if (onConfirm != null) {
            binding.btnConfirm.setOnClickListener(onConfirm);
        }

        return binding.getRoot();
    }

    public DialogBottomSheetMessageConfirm setTitle(String title) {
        this.title = title;
        return this;
    }

    public DialogBottomSheetMessageConfirm setDescription(String description) {
        this.description = description;
        return this;
    }

    public DialogBottomSheetMessageConfirm setDanger() {
        this.isDanger = true;
        return this;
    }

    public DialogBottomSheetMessageConfirm setOnCancelClickListener(View.OnClickListener onClickListener) {
        this.onCancel = onClickListener;
        return this;
    }

    public DialogBottomSheetMessageConfirm setOnConfirmClickListener(View.OnClickListener onClickListener) {
        this.onConfirm = onClickListener;
        return this;
    }

    public DialogBottomSheetMessageConfirm setConfirmText(String str) {
        this.confirmText = str;
        return this;
    }

    public DialogBottomSheetMessageConfirm setCancelText(String str) {
        this.cancelText = str;
        return this;
    }

    public void show(FragmentManager fragmentManager) {
        this.show(fragmentManager, null);
    }

}