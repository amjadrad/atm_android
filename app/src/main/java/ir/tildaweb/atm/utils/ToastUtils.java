package ir.tildaweb.atm.utils;

import android.content.Context;
import android.widget.Toast;


public class ToastUtils {

    public enum ToastType {
        NONE,
        SUCCESS,
        ERROR,
        SUCCESS_INSERT,
        SUCCESS_DELETE,
        SUCCESS_UPDATE,
        ERROR_INSERT,
        ERROR_DELETE,
        ERROR_UPDATE,
    }

    public static void toast(Context context, String message, ToastType toastType) {
        String result = message;
        switch (toastType) {
            case NONE:
                break;
            case SUCCESS:
                result = "با موفقیت انجام شد.";
                break;
            case ERROR:
                result = "مشکلی پیش آمد. مجددا تلاش کنید.";
                break;
            case SUCCESS_INSERT:
                result = "با موفقیت افزوده شد.";
                break;
            case SUCCESS_DELETE:
                result = "با موفقیت حذف شد.";
                break;
            case SUCCESS_UPDATE:
                result = "با موفقیت ویرایش شد.";
                break;
            case ERROR_INSERT:
                result = "مشکلی در افزودن " + message + " به وجود آمد.";
                break;
            case ERROR_DELETE:
                result = "مشکلی در حذف " + message + " به وجود آمد.";
                break;
            case ERROR_UPDATE:
                result = "مشکلی در ویرایش " + message + " به وجود آمد.";
                break;
        }

        Toast.makeText(context, result, Toast.LENGTH_LONG).show();
    }


}
